import communication.Communicator;
import datahandler.DataSaver;
import objects.DataObject;
import listener.CommandPromptListener;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static datahandler.DataSaver.setBufferSize;

public class Controller {

    public static String DATA_FOLDER_NAME = "sounds";
    public static String PORT_NAME = "COM4";

    public static void main(String[] args) {
        String currentDirectory = System.getProperty("user.dir");
        BlockingQueue<DataObject> queue = new LinkedBlockingQueue<>(100);


//        DataSaver dataSaver = new DataSaver("C:/Users/duur9/OneDrive/ProLab/DataSaver/sounds/" ,queue);
        //see äravõtta kui startCMDlistener tööle pannakse
        //    dataSaver.setListenToDataFlag(true);
        System.out.println("Insert port name(like COM4): ");
        PORT_NAME = System.console().readLine().toUpperCase();

        DataSaver dataSaver = new DataSaver(currentDirectory + "/" + DATA_FOLDER_NAME + "/", queue);
        if (!Files.isDirectory(Paths.get(currentDirectory + "/" + DATA_FOLDER_NAME))) {
            new File(currentDirectory + "/" + DATA_FOLDER_NAME).mkdirs();
        }

        if (!Files.isDirectory(Paths.get(currentDirectory + "/testsettings" ))) {
            new File(currentDirectory + "/testsettings").mkdirs();
        }
         /*
        File file = new File(currentDirectory + "/" + DATA_FOLDER_NAME + "/" + "config.txt");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } */
        soundSensorSettings(dataSaver, queue);

    }



    private static void soundSensorSettings(DataSaver dataSaver, BlockingQueue<DataObject> queue) {
        CommandPromptListener commandPromptListener = startCommandPromptListener(dataSaver);
        startCommunicator(queue, commandPromptListener);
        dataSaver.convertToShortArray();

    }

    private static CommandPromptListener startCommandPromptListener(DataSaver dataSaver) {
        CommandPromptListener commandPromptListener = new CommandPromptListener(dataSaver);
        Runnable runnable = commandPromptListener;
        Thread t = new Thread(runnable);
        t.start();
        return commandPromptListener;

    }

    private static void startCommunicator(BlockingQueue<DataObject> queue, CommandPromptListener commandPromptListener) {
        Communicator communicator = new Communicator(queue);
        communicator.setCommandPromptListener(commandPromptListener);
        try {
            communicator.connect(PORT_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
// https://raspberrypi.stackexchange.com/questions/36254/serial-communication-gives-incorrect-read
/* while (portList.hasMoreElements())
            {
            CommPortIdentifier  portId = (CommPortIdentifier) portList.nextElement();
                if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL)
                    {
                        //System.out.println ("Found " + portId.getName());
                        if (portId.getName().equals("COM5"))
                            {

                                SerialPort serialPort = (SerialPort) portId.open("SimpleWriteApp", 2000);
                                serialPort.setSerialPortParams(38400,
                                                              SerialPort.DATABITS_8,
                                                              SerialPort.STOPBITS_1,
                                                              SerialPort.PARITY_NONE); */







