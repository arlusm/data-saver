package objects;

import event.Event;

import java.util.ArrayList;
import java.util.List;

public class DataObject {
    private List<Integer> soundData = new ArrayList<>();
    private Event eventFlag = Event.UNINITIALIZED;
    private boolean saveImmediately = false;
    private String commentID = "";

    public List<Integer> getSoundData() {
        return soundData;
    }

    public void setSoundData(List<Integer> soundData) {
        this.soundData = soundData;
    }

    public Event getEventFlag() {
        return eventFlag;
    }

    public void setEventFlag(Event eventFlag) {
        this.eventFlag = eventFlag;
    }

    public boolean isSaveImmediately() {
        return saveImmediately;
    }

    public void setSaveImmediately(boolean saveImmediately) {
        this.saveImmediately = saveImmediately;
    }

    public String getCommentID() {
        return commentID;
    }

    public void setCommentID(String commentID) {
        this.commentID = commentID;
    }
}


