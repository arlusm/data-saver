package datahandler;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import objects.CommentObject;
import objects.DataObject;
import event.Event;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.BlockingQueue;

public class DataSaver {
    public static int BUFFER_SIZE = 61440;
    public static final int BREAK_BETWEEN_REQUESTS_TO_API_IN_SECONDS = 60;
    public static final int SEARCHABLE_RADIUS_IN_KM = 5;
    public static int SAMPLE_RATE = 10000;
    private String metaInfo = "";

    private boolean listenToDataFlag = false;

    private String path = "";

    private BlockingQueue<DataObject> queue;

    private DateFormat dateFormat = new SimpleDateFormat("yyMMdd_HHmmss");
    private DateFormat metaInfoFormat = new SimpleDateFormat("yy-MM-dd");

    private List<Integer> bufferedData = new ArrayList<>();
    private HashSet<Event> currentEvents = new HashSet<>();
    private List<String> planeICAOs = new ArrayList<>();
    private List<CommentObject> addedComments = new ArrayList<>();
    private List<DataObject> bufferedDataObject = new ArrayList<>();

    private LocalTime lastRequest;
    List<CommentObject> usedComments = new ArrayList<>();

    private double latitude = 59.394402;
    private double longitude = 24.672420;


    public DataSaver(String path, BlockingQueue<DataObject> queue) {
        this.queue = queue;
        if (path.charAt(path.length() - 1) != '/') {
            path += "/";
        }
        this.path = path;
    }

    public synchronized void addComment(CommentObject comment) {
        addedComments.add(comment);
    }

    boolean checkDirectoryAndWriteToFile(AudioInputStream content, Event event, List<String> planeICAOs,
                                         String commentIDs) {
        Date date = new Date();
        String currentTimeString = dateFormat.format(date);
        String eventDirectory  = path + event + "/";
        String filename = "sig_" + currentTimeString + ".wav";
        String fullFilePath = eventDirectory + filename;
        if (Files.isDirectory(Paths.get(eventDirectory))) {
            return writeToFile(content, fullFilePath,
                    filename, event, planeICAOs, commentIDs);
        } else {
            new File(eventDirectory).mkdirs();
            return writeToFile(content, fullFilePath,
                    filename, event, planeICAOs, commentIDs);
        }
    }

    private boolean writeToFile(AudioInputStream content, String path, String filename, Event event,
                                List<String> planeICAOs, String commentIDs) {
        try {
            AudioSystem.write(content, AudioFileFormat.Type.WAVE, new File(path));
            writeMetaInfoToFile(filename, event, planeICAOs, commentIDs);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void writeMetaInfoToFile(String filename, Event event, List<String> planeICAOs, String commentIDs) throws IOException {
        if (planeICAOs == null) planeICAOs = new ArrayList<>();
        if (!Files.isDirectory(Paths.get(path + "METAINFO"))) {
            new File(path + "METAINFO").mkdirs();
        }
        if (!getMetaInfo().equals("")) {
            File file = new File(path + "METAINFO/"  +
                    "/" + metaInfoFormat.format(new Date()) + ".txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            BufferedWriter out = new BufferedWriter(new FileWriter(file.getAbsoluteFile(), true));
            if (planeICAOs.isEmpty()) {
                out.write(filename + " " + getMetaInfo() + ";" + event.toString() + ";" + commentIDs + ";");
            } else {
                out.write(filename + " " + getMetaInfo() + ";" + event.toString() + ";" + planeICAOs.toString() +
                        commentIDs + ";");
            }
            out.newLine();
            out.close();
            if (!addedComments.isEmpty()) {
                Path path = Paths.get(file.getAbsolutePath());
                String metaInfoContent = new String(Files.readAllBytes(path));

                for (CommentObject co: addedComments) {
                    if (metaInfoContent.contains(co.getCommentID())) {
                        usedComments.add(co);
                        metaInfoContent = metaInfoContent.replaceAll(co.getCommentID(), co.getComment());
                        Files.write(path, metaInfoContent.getBytes());
                    }
                }
                addedComments.removeAll(usedComments);
            }

        }
        else {
            try {
                throw new Exception("Meta information not set!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    //heliradari jaoks
    public void convertToShortArray() {
        int frameCounter = 0;
        while (true) {
            if (isListenToDataFlag()) {
                DataObject soundData = null;
                try {
                    soundData = queue.take();

                    if (currentEvents.isEmpty() || currentEvents.contains(soundData.getEventFlag())) {
                        currentEvents.add(soundData.getEventFlag());
                        bufferedData.addAll(soundData.getSoundData());
                        bufferedDataObject.add(soundData);

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (soundData != null && currentEvents.iterator().next() != Event.UNINITIALIZED) {

                    if (bufferedData.size() >= BUFFER_SIZE || soundData.isSaveImmediately()) {
                        String commentIDs = "";
                        for (DataObject datObj : bufferedDataObject) {
                            if (!datObj.getCommentID().equals("")) {
                                commentIDs += datObj.getCommentID() + " ";
                            }
                        }
                        bufferedDataObject.clear();
                        if (!soundData.getEventFlag().equals(Event.PLANE) && !planeICAOs.isEmpty()) planeICAOs.clear();

                        if (soundData.getEventFlag().equals(Event.PLANE) &&
                                (lastRequest == null || lastRequest.until(LocalDateTime.now(), ChronoUnit.SECONDS)
                                        > BREAK_BETWEEN_REQUESTS_TO_API_IN_SECONDS)) {
                            lastRequest = LocalDateTime.now().toLocalTime();
                            System.out.println("Plane, finding nearby planes ICAO");
                            try {
                                Object[] data =
                                        calculateBoundingBoxLimits(getLongitude(), getLatitude(),
                                                SEARCHABLE_RADIUS_IN_KM);
                               /* planeICAOs = findNearbyPlanesICAO((double) data[0],(double) data[1],
                                        (double) data[2],(double) data[3]); */
                                planeICAOs = findNearbyPlanesICAO
                                        (59.399520, 24.648277, 59.551575, 24.922935);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        short[] shortArray = new short[bufferedData.size()];
                        for (int i = 0; i < bufferedData.size(); i++) {
                            shortArray[i] = bufferedData.get(i).shortValue();
                        }
                        saveAsWav(shortArray, currentEvents.iterator().next(), planeICAOs, commentIDs);
                        bufferedData.clear();
                        currentEvents.clear();
                    }
                }
            }
        }
    }


    public void saveAsWav(short[] shortData, Event event, List<String> planeICAOs, String commentIDs) {
        byte[] data;
        ByteBuffer byteBuffer = ByteBuffer.allocate(shortData.length * 2);
        for (short s : shortData) {
            byteBuffer.putShort(s);
        }
        data = byteBuffer.array();

        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        AudioFormat format = new AudioFormat(SAMPLE_RATE, 16, 1, false, true);
        AudioInputStream ais = new AudioInputStream(bais, format, shortData.length);
        checkDirectoryAndWriteToFile(ais, event, planeICAOs, commentIDs);
    }

    //https://opensky-network.org/aircraft-profile?icao24=502cc6

    //lamin=59.399520&lomin=24.648277&lamax=59.551575&lomax=24.922935
    private List<String> findNearbyPlanesICAO(double minLat, double minLon, double maxLat, double maxLon) throws IOException {
        List<String> nearbyPlaneICAOcodes = new ArrayList<>();

        URL url = new URL("https://opensky-network.org/api/states/all?" +
                "lamin=" + minLat + "&lomin=" + minLon + "&lamax=" + maxLat + "&lomax=" + + maxLon);
        URLConnection request = url.openConnection();
        request.connect();

        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));

        JsonObject rootObj = root.getAsJsonObject();
        if (rootObj.get("states").isJsonNull()) {
            return nearbyPlaneICAOcodes;
        }
        JsonArray statesArray = rootObj.getAsJsonArray("states");
        System.out.println(statesArray.toString());
        for (JsonElement separatePlaneData: statesArray) {
            //https://opensky-network.org/apidoc/rest.html#response
            //When the plane is not on ground i.e it's flying.
            if (separatePlaneData.getAsJsonArray().get(8).toString().equals("false")) {
                //then add the plane ICAO24 codes to the list
                nearbyPlaneICAOcodes.add(separatePlaneData.getAsJsonArray().get(0).toString());
            }
        }
        return nearbyPlaneICAOcodes;
    }

    private Object[] calculateBoundingBoxLimits(double lon, double lat, double radius) {
        double R = 6371;
        double minLat = lon - Math.toDegrees(radius/R/Math.cos(Math.toRadians(lat)));
        double minLon = lon + Math.toDegrees(radius/R/Math.cos(Math.toRadians(lat)));
        double maxLat = lat + Math.toDegrees(radius/R);
        double maxLon = lat - Math.toDegrees(radius/R);

        Object[] data = new Object[4];
        data[0] = minLat;
        data[1] = minLon;
        data[2] = maxLat;
        data[3] = maxLon;
        return data;
    }

    public synchronized String getMetaInfo() {
        return metaInfo;
    }

    public synchronized void setMetaInfo(String metaInfo) {
        this.metaInfo = metaInfo;
    }

    public synchronized boolean isListenToDataFlag() {
        return listenToDataFlag;
    }

    public synchronized void setListenToDataFlag(boolean listenToDataFlag) {
        this.listenToDataFlag = listenToDataFlag;
    }

    public synchronized double getLatitude() {
        return latitude;
    }

    public synchronized void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public synchronized double getLongitude() {
        return longitude;
    }

    public synchronized void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public static int getBufferSize() {
        return BUFFER_SIZE;
    }

    public static void setBufferSize(int bufferSize) {
        BUFFER_SIZE = bufferSize;
    }

    public static int getSampleRate() {
        return SAMPLE_RATE;
    }

    public static void setSampleRate(int sampleRate) {
        SAMPLE_RATE = sampleRate;
    }

}


