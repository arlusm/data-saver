package communication;
import objects.DataObject;
import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import listener.CommandPromptListener;

import java.io.InputStream;
import java.util.concurrent.BlockingQueue;

public class Communicator {

    private static final int TIMEOUT = 500;
    private BlockingQueue<DataObject> queue;
    private CommandPromptListener commandPromptListener;

    public Communicator(BlockingQueue<DataObject> queue) {
        this.queue = queue;
    }

    public void connect(String portName) throws Exception {
            CommPortIdentifier portIdentifier = CommPortIdentifier
                    .getPortIdentifier(portName);
            if (portIdentifier.isCurrentlyOwned()) {
                throw new Exception("Port currently in use");
            }
            CommPort commPort = portIdentifier.open("Data Saver", TIMEOUT);
            SerialPort serialPort = (SerialPort) commPort;
            serialPort.setSerialPortParams( 921600,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE );

            InputStream in = serialPort.getInputStream();
            Reader reader = new Reader(in, queue);
        commandPromptListener.setReader(reader);
        new Thread(reader).start();
            }

    public void setCommandPromptListener(CommandPromptListener commandPromptListener) {
        this.commandPromptListener = commandPromptListener;
    }
}
