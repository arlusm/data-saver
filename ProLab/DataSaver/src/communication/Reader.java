package communication;

import objects.DataObject;
import event.Event;

import java.io.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.BlockingQueue;


public class Reader implements Runnable {


    //framelength 4096 aga et sekundite täpsust täpsemaks saada siis panna 100
    public static final int FRAME_LENGTH = 100;
    public static final int LARGEST_ALLOWED_VALUE_IN_AUDIO = 4096;
    private InputStream in;
    private BlockingQueue<DataObject> queue;
    private boolean saveDataFlag = false;
    private Event currentEvent = Event.UNINITIALIZED;
    private LocalDateTime lastReceivedDate = LocalDateTime.now();
    private boolean timeWarningFlag = false;

    private String commentID = "";
    //TODO currentEvent pole thread-safe, vahepeal on uninitialized kui ei tohiks olla
    Reader(InputStream in, BlockingQueue<DataObject> queue) {
        this.queue = queue;
        this.in = in;
    }

    public void run() {
      gatherData();
    }

    public void gatherData() {
        DataInputStream dataInputStream = new DataInputStream(in);
        DataObject dataList = new DataObject();
        try {
            while(true) {
                if (ChronoUnit.SECONDS.between(lastReceivedDate, LocalDateTime.now()) > 3 && !timeWarningFlag) {
                    System.out.println("Data hasn't been forwarded to the port for over 3 seconds");
                    timeWarningFlag = true;
                }
                while (in.available() > 0) {
                    timeWarningFlag = false;
                    int twoBytesOfData = dataInputStream.readUnsignedShort();
                    dataList.getSoundData().add(twoBytesOfData);
                    lastReceivedDate = LocalDateTime.now();
                    if (!isSaveDataFlag()) dataList.setEventFlag(currentEvent);
                    if (!getCommentID().equals("")) {
                        dataList.setCommentID(commentID);
                        commentID = "";
                    }
                    if (twoBytesOfData > LARGEST_ALLOWED_VALUE_IN_AUDIO) {
                        dataInputStream.skipBytes(1);
                        dataList.getSoundData().remove(dataList.getSoundData().size() - 1);
                    }
                    if (isSaveDataFlag()) {
                        dataList.setSaveImmediately(true);
                        queue.put(dataList);
                        dataList = new DataObject();
                        dataList.setEventFlag(currentEvent);
                        setSaveDataFlag(false);
                    }
                    if (dataList.getSoundData().size() == FRAME_LENGTH) {
                        queue.put(dataList);
                        dataList = new DataObject();
                        dataList.setEventFlag(currentEvent);
                    }
                }
            }
        } catch (IOException | InterruptedException e) {
            gatherData();
        }
    }

    public synchronized String getCommentID() {
        return commentID;
    }

    public synchronized void setCommentID(String commentID) {
        this.commentID = commentID;
    }

    public synchronized boolean isSaveDataFlag() {
        return saveDataFlag;
    }

    public synchronized void setSaveDataFlag(boolean saveDataFlag) {
        this.saveDataFlag = saveDataFlag;
    }

    public Event getCurrentEvent() {
        return currentEvent;
    }

    public void setCurrentEvent(Event currentEvent) {
        this.currentEvent = currentEvent;
    }
}