package event;

public enum Event {
    CAR,
    PLANE,
    BUS,
    NONE,
    UNKNOWN,
    UNINITIALIZED
}
